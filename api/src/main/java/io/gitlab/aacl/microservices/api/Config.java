package io.gitlab.aacl.microservices.api;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.consul.RetryProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.data.r2dbc.function.DatabaseClient;
import org.springframework.data.r2dbc.repository.support.R2dbcRepositoryFactory;
import org.springframework.data.relational.core.mapping.RelationalMappingContext;
import org.springframework.retry.interceptor.RetryInterceptorBuilder;
import org.springframework.retry.interceptor.RetryOperationsInterceptor;

@Configuration
@EnableAutoConfiguration
@ComponentScan("io.gitlab.aacl.microservices.api")
public class Config {
    @Bean
    @ConditionalOnMissingBean(name = "configServerRetryInterceptor")
    public RetryOperationsInterceptor configRetry(RetryProperties properties) {
        return RetryInterceptorBuilder
                .stateless()
                .backOffOptions(
                        properties.getInitialInterval(),
                        properties.getMultiplier(),
                        properties.getMaxInterval()
                )
                .maxAttempts(properties.getMaxAttempts())
                .build();
    }

    @Bean
    public PostgresqlConnectionFactory postgresqlConnectionFactory(PostgresConfig config) {
        return new PostgresqlConnectionFactory(
                PostgresqlConnectionConfiguration.builder()
                        .host(config.getHost())
                        .database(config.getDb())
                        .username(config.getUsername())
                        .password(config.getPassword())
                        .port(config.getPort())
                        .build()
        );
    }

    @Bean
    public DatabaseClient databaseClient(PostgresqlConnectionFactory connectionFactory ) {
        return DatabaseClient.create(connectionFactory);
    }

    @Bean
    public MappingContext mappingContext() {
        final RelationalMappingContext relationalMappingContext = new RelationalMappingContext();
        relationalMappingContext.afterPropertiesSet();
        return relationalMappingContext;
    }

    @Bean
    public R2dbcRepositoryFactory repositoryFactory(DatabaseClient client, MappingContext context) {
        return new R2dbcRepositoryFactory(client, context);
    }


    @Bean
    public ReviewRepository reactiveCoffeeRepository(R2dbcRepositoryFactory repositoryFactory) {
        return repositoryFactory.getRepository(ReviewRepository.class);
    }
}
